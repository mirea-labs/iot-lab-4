package ru.svlit.chat

import android.app.Application
import org.eclipse.paho.client.mqttv3.MqttClient
import ru.svlit.chat.di.MqttRepositoryFactory

class MqttApplication : Application() {

    lateinit var mqttRepositoryFactory: MqttRepositoryFactory
    val clientId: String = MqttClient.generateClientId()

    override fun onCreate() {
        super.onCreate()
        mqttRepositoryFactory = MqttRepositoryFactory(this, clientId)
    }
}