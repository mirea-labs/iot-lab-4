package ru.svlit.chat.data

import android.content.Context
import android.util.Log
import com.google.gson.Gson
import io.reactivex.Completable
import io.reactivex.Completable.fromAction
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import org.eclipse.paho.android.service.MqttAndroidClient
import org.eclipse.paho.client.mqttv3.*
import ru.svlit.chat.model.data.MessageData


class MqttRepository(
    context: Context,
    server: String,
    private val clientId: String,
    private val username: String,
    private val password: String
) {
    private val gson = Gson()

    private val messageSubject = PublishSubject.create<MessageData>()

    private val mqttClient = MqttAndroidClient(context, server, clientId)

    private val messageListener = IMqttMessageListener { _, message ->
        val json = message.toString()
        messageSubject.onNext(
            gson.fromJson<MessageData>(json, MessageData::class.java)
        )
    }

    init {
        connect()
    }

    private fun connect() {
        val mqttConnectOptions = MqttConnectOptions().apply {
            isAutomaticReconnect = true
            isCleanSession = false
            userName = username
            password = this@MqttRepository.password.toCharArray()
        }

        mqttClient.connect(mqttConnectOptions, null, object : IMqttActionListener {
            override fun onSuccess(asyncActionToken: IMqttToken?) {
                DisconnectedBufferOptions().apply {
                    isBufferEnabled = true
                    bufferSize = 100
                    isPersistBuffer = false
                    isDeleteOldestMessages = false
                    mqttClient.setBufferOpts(this)
                }
            }

            override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
                Log.d(TAG, "Error connecting to $mqttClient")
            }

        })
    }

    fun subscribe(topic: String): Observable<MessageData> {
        mqttClient.subscribe(topic, 0, messageListener)
        return messageSubject.filter { it.topic == topic }
    }

    fun unsubscribe(topic: String): Completable = fromAction {
        mqttClient.unsubscribe(topic)
    }

    fun publish(message: MessageData) {
        val mqttMessage = MqttMessage(gson.toJson(message.copy(clientId = clientId)).toByteArray())
        mqttClient.publish(message.topic, mqttMessage)
    }

    companion object {
        private const val TAG = "MqttRepository"
    }
}