package ru.svlit.chat.presentation.chat.adapter

import android.view.View
import android.widget.TextView
import ru.svlit.chat.R
import ru.svlit.chat.model.presentation.MessagePresentation

class MessageViewHolder(private val view: View) : BaseViewHolder(view) {

    private val authorTextView: TextView?
    private val contentsTextView: TextView?
    private val timeTextView: TextView?

    init {
        view.apply {
            authorTextView = findViewById(R.id.message_author_text_view)
            contentsTextView = findViewById(R.id.message_text_text_view)
            timeTextView = findViewById(R.id.message_date_text_view)
        }
    }

    override fun <T : BaseItem> bind(item: T) {
        if (item !is MessagePresentation) {
            return
        }

        view.apply {
            authorTextView?.text = item.author
            contentsTextView?.text = item.message
            timeTextView?.text = item.date
        }
    }

}
