package ru.svlit.chat.presentation.chat.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ru.svlit.chat.R
import ru.svlit.chat.model.presentation.MessagePresentation


class ChatAdapter : RecyclerView.Adapter<BaseViewHolder>() {

    val messages: MutableList<MessagePresentation> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(
            if (viewType == TYPE_MESSAGE_MY) R.layout.item_message_own else R.layout.item_message,
            parent,
            false
        )

        return MessageViewHolder(view)
    }

    override fun getItemViewType(position: Int): Int {
        return if (messages[position].isMine) TYPE_MESSAGE_MY else TYPE_MESSAGE_OTHER
    }

    override fun getItemCount() = messages.size

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.bind(messages[position])
    }


    companion object {
        private const val TYPE_MESSAGE_MY = 1
        private const val TYPE_MESSAGE_OTHER = 2
    }
}
