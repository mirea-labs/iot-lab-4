package ru.svlit.chat.presentation.chat

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager.VERTICAL
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import ru.svlit.chat.MqttApplication
import ru.svlit.chat.R
import ru.svlit.chat.data.MqttRepository
import ru.svlit.chat.model.converter.MessageDataToPresentationConverter
import ru.svlit.chat.model.data.MessageData
import ru.svlit.chat.presentation.chat.adapter.ChatAdapter

class ChatFragment : Fragment() {

    private val handler = Handler()

    private lateinit var author: String
    private lateinit var server: String
    private var port: Int = -1
    private lateinit var username: CharSequence
    private lateinit var password: CharSequence
    private lateinit var topic: String


    private lateinit var chatRecyclerView: RecyclerView
    private lateinit var messageEditText: EditText
    private lateinit var sendButton: Button

    private lateinit var adapter: ChatAdapter
    private lateinit var mqttRepository: MqttRepository
    private lateinit var converter: MessageDataToPresentationConverter
    private var compositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        adapter = ChatAdapter()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_chat, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        showBackButton()

        view.apply {
            chatRecyclerView = findViewById(R.id.chat_recycler_view)
            messageEditText = findViewById(R.id.message_text_input_edit_text)
            sendButton = findViewById(R.id.send_button)
        }

        chatRecyclerView.adapter = adapter
        chatRecyclerView.layoutManager =
            LinearLayoutManager(requireContext(), VERTICAL, false).apply {
                stackFromEnd = true
            }

        (requireContext().applicationContext as MqttApplication).apply {
            converter = MessageDataToPresentationConverter(clientId)
            mqttRepository = mqttRepositoryFactory.create(
                server,
                port,
                username,
                password
            )
        }
    }

    private fun showBackButton() {
        (requireActivity() as AppCompatActivity).supportActionBar
            ?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onResume() {
        super.onResume()
        handler.postDelayed({
            sendButton.setOnClickListener { onSendClicked() }
            subscribeToTopic()
        }, DELAY)
    }

    override fun onPause() {
        super.onPause()
        sendButton.setOnClickListener(null)
        unsubscribeFromTopic()
    }

    private fun subscribeToTopic() {
        val disposable = mqttRepository.subscribe(topic)
            .map(converter::convert)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { message ->
                adapter.messages.add(message)
                adapter.notifyDataSetChanged()
                chatRecyclerView.scrollToPosition(adapter.messages.size - 1)

            }

        compositeDisposable.add(disposable)
    }

    private fun unsubscribeFromTopic() {
        mqttRepository.unsubscribe(topic)
        compositeDisposable.clear()
    }

    private fun onSendClicked() {
        val text = messageEditText.text.toString()
        if (text.isNotBlank()) {
            messageEditText.text.clear()

            val message = MessageData(
                topic,
                author,
                text,
                System.currentTimeMillis(),
                ""
            )

            mqttRepository.publish(message)
        }
    }

    companion object {
        private const val DELAY = 500L

        fun newInstance(
            author: String,
            server: String,
            port: Int,
            username: CharSequence,
            password: CharSequence,
            topic: String
        ): ChatFragment {
            return ChatFragment().apply {
                this.author = author
                this.topic = topic
                this.server = server
                this.port = port
                this.username = username
                this.password = password
            }
        }
    }
}