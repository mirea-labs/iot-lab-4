package ru.svlit.chat.presentation.chat.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class BaseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    abstract fun <T : BaseItem> bind(item: T)
}
