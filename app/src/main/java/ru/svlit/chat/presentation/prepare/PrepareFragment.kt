package ru.svlit.chat.presentation.prepare

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.android.material.textfield.TextInputEditText
import ru.svlit.chat.R
import ru.svlit.chat.presentation.ContainerActivity
import ru.svlit.chat.presentation.chat.ChatFragment

class PrepareFragment : Fragment() {

    private lateinit var author: String
    private lateinit var server: String
    private var port: Int? = null
    private lateinit var user: CharSequence
    private lateinit var password: CharSequence
    private lateinit var topic: String

    private lateinit var authorEditText: TextInputEditText
    private lateinit var serverEditText: TextInputEditText
    private lateinit var portEditText: TextInputEditText
    private lateinit var userEditText: TextInputEditText
    private lateinit var passwordEditText: TextInputEditText
    private lateinit var topicEditText: TextInputEditText

    private lateinit var startChattingButton: Button

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_prepare, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        hideBackButton()

        view.apply {
            authorEditText = findViewById(R.id.prepare_author_text_input_edit_text)
            serverEditText = findViewById(R.id.prepare_server_text_input_edit_text)
            portEditText = findViewById(R.id.prepare_port_text_input_edit_text)
            userEditText = findViewById(R.id.prepare_username_text_input_edit_text)
            passwordEditText = findViewById(R.id.prepare_password_text_input_edit_text)
            topicEditText = findViewById(R.id.prepare_topic_text_input_edit_text)
            startChattingButton = findViewById(R.id.prepare_start_chatting_button)
        }

        prefill()
    }

    private fun prefill() {
        authorEditText.setText(author)
        serverEditText.setText(server)
        portEditText.setText(port?.toString() ?: "")
        userEditText.setText(user.toString())
        passwordEditText.setText(password.toString())
        topicEditText.setText(topic)
    }

    override fun onResume() {
        super.onResume()
        startChattingButton.setOnClickListener { startChat() }
    }

    override fun onPause() {
        super.onPause()
        startChattingButton.setOnClickListener(null)
    }

    private fun startChat() {
        val author = authorEditText.text?.toString() ?: return
        val server = serverEditText.text?.toString() ?: return
        val port = portEditText.text?.toString()?.toInt() ?: return
        val user = userEditText.text?.toString() ?: return
        val password = passwordEditText.text?.toString() ?: return
        val topic = topicEditText.text?.toString() ?: return

        (requireActivity() as ContainerActivity).addFragment(
            ChatFragment.newInstance(
                author,
                server,
                port,
                user,
                password,
                topic
            )
        )
    }

    private fun hideBackButton() {
        (requireActivity() as AppCompatActivity).supportActionBar
            ?.setDisplayHomeAsUpEnabled(false)
    }

    companion object {
        fun newInstance(
            author: String = "",
            server: String = "",
            port: Int? = null,
            username: CharSequence = "",
            password: CharSequence = "",
            topic: String = ""
        ) = PrepareFragment().apply {
            this.author = author
            this.server = server
            this.port = port
            this.user = username
            this.password = password
            this.topic = topic
        }
    }
}