package ru.svlit.chat.presentation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import ru.svlit.chat.R
import ru.svlit.chat.presentation.prepare.PrepareFragment

class ContainerActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        addFragment(
            PrepareFragment.newInstance(
                author = "Sergei Litvinenko",
                server = "farmer.cloudmqtt.com",
                port = 12138,
                username = "ogpmiega",
                password = "bHF7a57PC67Q",
                topic = "kitten"
            )
        )
    }

    override fun onSupportNavigateUp(): Boolean {
        supportFragmentManager.popBackStack()
        return true
    }

    fun addFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .addToBackStack(fragment.tag)
            .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
            .replace(R.id.activity_container, fragment)
            .commit()
    }
}
