package ru.svlit.chat.model.presentation

import ru.svlit.chat.presentation.chat.adapter.BaseItem

data class MessagePresentation(
    val author: String,
    val message: String,
    val date: String,
    val isMine: Boolean
) : BaseItem()