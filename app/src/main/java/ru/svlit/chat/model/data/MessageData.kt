package ru.svlit.chat.model.data

import com.google.gson.annotations.SerializedName

data class MessageData(
    @SerializedName("topic") val topic: String,
    @SerializedName("author") val author: String,
    @SerializedName("message") val message: String,
    @SerializedName("timestamp") val timestamp: Long,
    @SerializedName("client_id") val clientId: String
)