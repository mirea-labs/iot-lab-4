package ru.svlit.chat.model.converter

import ru.svlit.chat.model.data.MessageData
import ru.svlit.chat.model.presentation.MessagePresentation
import java.text.SimpleDateFormat
import java.util.*

class MessageDataToPresentationConverter(
    private val myClientId: String
) {

    fun convert(from: MessageData): MessagePresentation {
        return MessagePresentation(
            author = from.author,
            message = from.message,
            date = SimpleDateFormat.getTimeInstance().format(Date(from.timestamp)),
            isMine = myClientId == from.clientId
        )
    }

}
