package ru.svlit.chat.di

import android.content.Context
import ru.svlit.chat.data.MqttRepository

class MqttRepositoryFactory(
    private val context: Context,
    private val clientId: String
) {
    fun create(server: String, port: Int, username: CharSequence, password: CharSequence) =
        MqttRepository(
            context,
            "tcp://$server:$port",
            clientId,
            username.toString(),
            password.toString()
        )
}